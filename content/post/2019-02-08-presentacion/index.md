---
title: ¡Estrenamos nueva página web!
description: 
date: 2019-02-08
---

Desde SUGUS estamos contentos de poder mostrar lo que vendrá a ser la nueva web 
de la Asociación. Utilizando [Hugo](https://gohugo.io/) como generador estático 
de contenido, alojando el contenido en GitLab y usando la herramienta de 
Integración Continua que la misma web ofrece.

## ¿Cómo funciona esta nueva web?

Puedes encontrar el repositorio con el contenido de la SugusWeb en
[GitLab](https://gitlab.com/sugus_gitlab/sugusweb/). Esto es el directo base de
trabajo para una página web que usa Hugo. Mejoramos la facilidad con la que se
puede contribuir a la página gracias a la Integración Continua de GitLab pues
la construcción de la página y el despliegue en el servidor de la Asociación es
automática.

## Objetivos de la nueva web

Nuestro objetivo con esta nueva página es reducir la cantidad de
infraestructura a mantener a la vez que facilitamos la participación de
cualquier persona interesada en la misma, socios, ponentes, etc.

## Qué podrás encontrar en la nueva web

Por supuesto seguiremos anunciando todos aquellos eventos que organizamos desde
SUGUS, con la buena noticia de que ahora los integrantes de la asociación
podrán escribir artículos sobre aquellas tecnologías libres de su interés o
experiencias relacionadas para compartirlo con el resto de la comunidad.

## ¿Cómo puedo escribir un artículo?

Todas las cuestiones respecto a como contribuir a la página web se documentarán
dentro del propio repositorio (en el README y ficheros en una carpeta `docs/`,
dentro de poco podrás encontrar más información al respecto. Si estás 
interesado, de mientras puedes leer una pequeña introducción a Hugo para
entender como escribir contenido [aquí](https://gohugo.io/getting-started/quick-start/)
y también sobre Markdown [aquí](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

## ¿bsd.eii.us.es?

Por ahora, mantenemos esta nueva web en el subdominio `bsd.eii.us.es` si bien
esperamos que en menos de un mes `sugus.eii.us.es` apunte a esta web. Este
cambio forma parte de nuestro plan para renovar nuestro servidor CREMA y cómo
gestionamos nuestros servicios alojados en el mismo. Lo más seguro es que
escribamos entradas al respecto para compartir con vosotros cómo lo hemos
hecho.

---

¿Te gustaría pasarte por Sugus? Estamos en la sala AS43 de la Escuela Técnica
Superior de Ingeniería Informática de la Universidad de Sevilla. ¡Te esperamos!
