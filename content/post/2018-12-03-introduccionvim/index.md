---
title: Taller de Introducción a Vim
description: ¡Aprende a escribir como una bala y en cualquier lado gracias a la potencia de Vim!
date: 2018-12-03
image: cartel-introduccionvim.jpg
categories: 
    - Actividades
tags: 
    - talleres
---

Comienza a usar Vim desde cero: desde como moverte sin despegar los dedos del teclado hasta como hacer ediciones con solo una tecla. 

- Ponente: José María Guisado Gómez
- 11 de diciembre de 2018
- Aula A0.30
