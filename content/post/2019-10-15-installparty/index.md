---
title: Linux Install Party - Octubre 2019
description: Ven este miércoles 16 al aula A2.14 y descubre el sistema operativo GNU/Linux mientras pruebas una distribución en tu propio ordenador.
date: 2019-10-15
image: cartel-installparty.png
slug: linux-install-party
categories:
    - Actividades
tags:
    - talleres
---

Linux Install Party es un taller donde te ayudamos a explorar el mundo de las distribuciones Linux y a instalar la que más te guste en tu pc. ¿Quieres instalarlo en dual-boot junto tu sistema actual? ¿Quieres probarlo en una maquina virtual? ¡Sin problema! Los compañeros de SUGUS estaremos resolviendo las dudas que tengais y aconsejando durante todo el evento.

Recomendamos que tengais preparada una copia de seguridad de vuestros archivos antes de la instalación: asi podeis pasar las cosas fácilmente al nuevo sistema y estais seguros si algo saliera mal ;)

Si teneis alguna duda podeis preguntarnos en nuestra cuenta de Twitter: [@sugus_etsii](https://twitter.com/sugus_etsii)

¡Os esperamos!
