---
title: RedHat Troubleshooting Competition
description: ¿Alguna vez te has enfrentado a alguna situación real con Linux en la que tú eras la única persona que podía y debía resolverla? No te pierdas esta "Troubleshooting Master Competition" 
date: 2019-03-08
image: cartel-redhat-troubleshooting.png
categories: 
    - Actividades
tags: 
    - talleres
---

En este taller encontrarás problemas tales como sistemas Linux que no arrancan,
páginas web reales que no funcionan, corrupciones de sistemas, problemas de
configuración... ¡Participa, disfruta, aprende y gana premios!
