---
title: Linux Install Party - Noviembre 2021
description: Descubre (y redescubre) el sistema operativo GNU/Linux usando tu propio PC en esta nueva edición del taller.
date: 2021-10-26
lastmod: 2021-10-28
image: cartel-installparty.png
slug: linux-install-party
categories:
    - Actividades
tags:
    - talleres
---

Así es, ¡SUGUS está de vuelta!  Y que mejor forma de volver a empezar que con un gran clásico como lo es la Linux Install Party.

## ¿Qué es una Linux Install Party?

Es un taller donde experimentamos que nos ofrece el sistema operativo GNU/Linux probando distintas distribuciones en todos sus colores y sabores. Una actividad perfecta para descubrir por primera vez  el mundo de Linux en tu propio ordenador mientras recibes ayuda y consejos del resto de compañeros, pero también es un momento excelente para quienes tengáis más rodaje podáis trastear con las ultimas novedades y compartir experiencias.

Aunque el nombre de *Linux Install Party* sugiere que para probar de primera mano hace falta instalar directamente el nuevo sistema en tu equipo realmente hay muchas opciones como:

- Instalar la nueva distribución Linux junto tu sistema habitual (lo que se conoce como [Dual Boot](https://es.wikipedia.org/wiki/Multiarranque))

- Instalarla en una máquina virtual

- Probando directamente desde un [Live USB](https://es.wikipedia.org/wiki/Live_USB)

- Y por supuesto, ¡instalando directamente el nuevo sistema en tu equipo! **😎**

¿No sabes cual elegir? ¡Sin problema! Puedes resolver con los compañeros tus dudas y escoger la que más te interese. O incluso probar varias.

## Horarios y aula

- Martes 2 de noviembre de 2021 a las 19:30

- Escuela Técnica Superior de Ingeniería Informática

- Aula: F0.10

## Notas importantes para el taller

Recomendamos que quienes queráis instalar una distribución en vuestro PC llevéis preparada una copia de seguridad de vuestros archivos. De esta forma es mucho más sencillo pasar vuestras cosas al nuevo sistema y como se suele decir: ¡más vale prevenir que curar!
