---
title: Taller de interfaces virtuales
description: 
date: 2021-11-04
image: cartel-interfacesvirtuales.png
categories:
    - Actividades
tags:
    - talleres
---

## Materiales necesarios

En este taller se trabajará con una **máquina virtual con CentOS Stream**, por lo que se recomienda llevarla preparada previamente.

## Horarios y aula

- Miércoles 10 de noviembre de 2021 a las 17:30

- Escuela Técnica Superior de Ingeniería Informática

- Aula: B1.35

## Ponente

El taller lo imparte Fernando Fernandez, software engineer en [Red Hat](https://www.redhat.com/es) y miembro de SUGUS.
