---
title: Sobre SUGUS
description: Tu asociación universitaria de software libre y código abierto en la Universidad de Sevilla.
date: '2019-09-30'
slug: about
aliases:
  - about-us
  - about-sugus
  - contact
lastmod: '2021-10-11'
menu:
    main:
        weight: -90
        params:
            icon: user
---

## ¿Dónde estamos?

Escuela Técnica de Ingeniería Informática US, Sótano [AS.43]

## ¿Qué hacemos?

Realizamos numerosas actividades con el objetivo de *fomentar el uso y la participación en el Software Libre* dentro de la Universidad: realizamos talleres sobre la utilización de herramientas libres, impartimos charlas, colaboramos en eventos de la Universidad, etc.

Por otro lado también trabajamos por la *defensa de los derechos digitales* y la *libertad en la red*,  mediante actividades tanto en la universidad como en el Parlamento Europeo.

Puedes pasar por SUGUS cuando quieras, siempre estamos dispuestos a hablar o echarte una mano :)

## Algunos proyectos libres donde han trabajado compañeros

### Propios

* [Minolobot](https://github.com/SUGUS-GNULinux/minolobot)
* [Flameshot](https://flameshot.js.org/)
* [Solaria](https://gitlab.com/atorresm/solaria)

### Colaborando

* [Netfilter](https://www.netfilter.org/)
* [Tor](https://www.torproject.org/)
* [Godot Engine](https://godotengine.org/)

## Proyectos en los que colabora SUGUS

* [Concurso Universitario de Software Libre (CUSL)](https://www.concursosoftwarelibre.org)
